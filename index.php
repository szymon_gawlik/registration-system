<?php
/**
 * Created by PhpStorm.
 * User: Komando
 * Date: 25.08.15
 * Time: 11:44
 */
?>
<!DOCTYPE html>
<html lang="pl" ng-app="App">
    <head>
        <title>Ewidencja</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="web/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="web/js/jquery-2.1.4.min.js" type="text/javascript"></script>

        <!-- Latest compiled JavaScript -->
        <script src="web/js/bootstrap.min.js"></script>
        <script src="web/js/angular.min.js"></script>
        <script src="web/js/angular-route.min.js"></script>
        <script src="web/js/ngDialog.min.js"></script>
        <script src="web/js/ngAutocomplete.js"></script>

        <link rel="stylesheet" href="web/css/ngDialog.css">
        <link rel="stylesheet" href="web/css/ngDialog-theme-default.min.css">
        <link rel="stylesheet" href="web/css/ngDialog-theme-plain.min.css">
        <script src="web/js/myApp.js"></script>
    </head>
<body ng-controller="MainCtrl">
    <div class="container" ng-controller="GetProducts">
        <div class="row col-md-6">
            <load-content site="menu"></load-content>
        </div>

        <div class="row col-md-6 pull-right" style="margin-top: 20px;">
            <label>Wyszukaj:
            <input ng-model="searchText"></label>
        </div>

        <load-content site="main-content"></load-content>
    </div>
</body>
</html>