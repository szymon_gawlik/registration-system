var app = angular.module('App', ['ngRoute','ngDialog','ngAutocomplete']);

app.controller('MainCtrl', function($scope, ngDialog) {
    $scope.openEdit = function(name,number,id,desc) {
         $scope.name = name;
         $scope.id = id;
         $scope.number = number;
         $scope.desc = desc;

        ngDialog.open({
            template: 'web/template/edit.html',
            scope: $scope
        });
   };
    $scope.openWindow = function(name) {
        ngDialog.open({
            template: 'web/template/windows/' + name + '.html',
            scope: $scope
        })
    };
    $scope.changeData = function(data) {
        $scope.products = data;
    };
});

app.controller('GetProducts', function($rootScope, $scope, $http, ngDialog) {
    $http.get('inc/fetchData.php').
        success(function(data) {
            $rootScope.products = data;
    });
    $scope.addOne = function(id){
        $.ajax({
            url: 'inc/addOne.php',
            method: 'post',
            data: {id: id}
        }).done(function(msg){
            $scope.return2 = angular.fromJson(msg);
            var i = 0;
            var id;
            $scope.products.forEach(function (product) {
                if(product.id  == $scope.return2[0].id) {
                    id = i;
                }
                i++;
            });

            //console.info($scope.return2[0].id);
            $scope.$apply(function(){
                $scope.products[id].number = $scope.return2[0].number;
            });
        });
    };
    $scope.minusOne = function(id){
        $.ajax({
            url: 'inc/minusOne.php',
            method: 'post',
            data: {id: id}
        }).success(function(msg){
            $scope.returnJson = angular.fromJson(msg);
            var i =0;
            var key;
            $scope.products.forEach(function(product){
                if($scope.returnJson[0].id === product.id) {
                    key = i;
                }
                i++;
            });

            $scope.$apply(function () {
                $scope.products[key].number = $scope.returnJson[0].number;
            });
        });
    };
    $scope.description = function(id){
       $rootScope.products.forEach(function(product){
            if(product.id == id) {
                $scope.descProduct = product;
            }
        });
        ngDialog.open({
            template: 'web/template/description-product.html',
            scope: $scope
        });
    };

    $scope.Customers = function () {
        $http.get('inc/getCustomers.php').success(function (data) {
             $scope.customers = angular.fromJson(data);
        });

        ngDialog.open({
            template: 'web/template/customers.html',
            scope:  $scope
        });
    }
});

app.controller('GetCategories', function($rootScope, $scope, $http, ngDialog){
    $rootScope.filterCategory = '';
    $http.get('inc/category.php').success(function(data){
            $rootScope.categories = data;
        });

    $scope.setCategoryKey = function(){
        $http.get('inc/setCategoryKey.php').
            success(function(categoryKey){
                $scope.category_key = categoryKey;
            });

    };
    $scope.addCategory = function(){
        var name = $('#name').val(),
            category_key = $('#category_key').val();

        $.ajax({
            url: 'inc/addCategory.php',
            type: 'post',
            data: {name: name, key: category_key}
        }).success(function(msg){
            var newData = angular.fromJson(msg);
            $scope.$apply(function(){
                $rootScope.categories.push(newData);
            });
            $('#alert').html('<div class="alert alert-success fade-in flash"><span class="glyphicon glyphicon-ok-circle"></span> Poprawnie dodano kategorie!' +
                '<a href="#" class="close" data-dimiss="alert" aria-label="close">&times;</a> </div>');

            ngDialog.close(this);
        });
    };
    $scope.AddCustomer = function () {
        var formName = document.getElementById('addCustomer'),
            formData = new FormData(formName);

        $.ajax({
            url: 'inc/addCustomer.php',
            type: 'post',
            data: formData,
            contentType: false,
            cache: false,
            processData: false
        }).success(function(msg){
            var newData = angular.fromJson(msg);
            $('#alert').html('<div class="alert alert-success fade-in flash"><span class="glyphicon glyphicon-ok-circle"></span> ' + newData.error + '<a href="#" class="close" data-dimiss="alert" aria-label="close">&times;</a> </div>');

            ngDialog.close(this);
        });
    };
    $scope.productCategory = function () {
        var chosenProduct = new Object(),
            category = $("#category").val(),
            i=0;

        $rootScope.products.forEach(function(product){
            if(product['category'] == category) {
                chosenProduct[i] = product;
                i++;
            }
        });
        $scope.chosenProduct = chosenProduct;
    };
    $scope.checkIfExist = function () {
        var inputNumber = $("#number").val(),
            productId = $("#item").val(),
            product1;
        $rootScope.products.forEach(function (product) {
            if(product['id'] == productId){
                product1 = product;
            }
        });
        if(inputNumber > product1.number){
            $('#alert').html('<div class="alert alert-danger fade-in flash"><span class="glyphicon glyphicon-remove-circle"></span> <strong>Nie ma wystarczającej ilości produktów!</strong>' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
            $scope.setNumber1 = 0;
        } else {
            $('#alert').html('');
            $scope.setNumber1 = 1;
        }
    }
});

app.controller('AddProduct', function($rootScope,$scope,ngDialog){
    $scope.addProduct = function(){
        var img = $('#upload')[0],
            formData = new FormData(img);

        $.ajax({
            url: 'inc/addProduct.php',
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false
        }).success(function(data) {
            var newData = angular.fromJson(data);
            if(!newData.error) {
                $scope.$apply(function(){
                    $rootScope.products.push(newData);
                });
                $('#alert').html('<div class="alert alert-success fade-in flash"><span class="glyphicon glyphicon-ok-circle"></span> <strong>Poprawnie dodano przedmiot!</strong>' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                ngDialog.close(this);

            } else {
                $('#alert').html('<div class="alert alert-danger fade-in">'+ newData.error +'</div>');
            }

        });
    };
});

app.controller('EditProduct', function($rootScope, $scope, ngDialog){
    $scope.edit = function(){
        var name = $('#name').val(),
             category = $('#category').val(),
             id = $('#id').val(),
             number = $('#number').val(),
             desc = $('#desc').val();

        $.ajax({
            url: 'inc/edit.php',
            method: 'post',
            data: {name: name, category: category, id:id, number:number, desc: desc}
        }).success(function(data){
            var newData = angular.fromJson(data),
                i = 0;
            $rootScope.products.forEach(function(product){
                if(product['id'] == newData['id']) {
                    $scope.$apply(function(){
                        $rootScope.products[i] = newData;
                    });
                }
                i++;
            });
            $('#alert').html('<div class="alert alert-success fade-in flash"><span class="glyphicon glyphicon-ok-sign"></span> <strong>Edycja przebiegła pomyślnie!</strong>' +
                '<a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>');
            ngDialog.close();
        });
    };
    $scope.remove = function(id) {
        var index;
        for (var i = 0; i < $rootScope.products.length; i++){
            if($rootScope.products[i]['id'] == id) {
                index = i;
            }
        }
        $.ajax({
            url: 'inc/remove.php',
            type: 'post',
            data: {id: id}
        }).success(function(data){
            $rootScope.products.splice(index,1);
            $scope.$apply(function(){
                $scope.alert = data;
                $scope.closeThisDialog('close');
            });
        })

    }
});

window.setTimeout(function(){
    $('.flash').fadeTo(500, 0).slideUp(800, function(){
        $(this).remove()
    });
},4000);

app.directive('loadContent', function () {
    return {
        templateUrl: function (elem, attr) {
            return 'web/template/' + attr.site + '.html';
        }
    }
});