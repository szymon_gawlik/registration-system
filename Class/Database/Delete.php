<?php
/**
 * Created by PhpStorm.
 * User: Komando
 * Date: 25.08.15
 * Time: 11:49
 */
namespace Database;

class Delete extends Database
{
    private $table;
    private $id;

    public function __construct($table, $id)
    {
        $this->table = $table;
        $this->id = $id;
        parent::__construct();
        $this->delete();
    }

    protected function delete()
    {
        $q = "DELETE FROM $this->table WHERE id=?";
        if($stmt = $this->conn->prepare($q)) {
            $stmt->bind_param('i', $this->id);
            $stmt->execute();
        } else {
            throw new \Exception('Błąd usuwania!');
        }
    }
}