<?php
/**
 * Created by PhpStorm.
 * User: Komando
 * Date: 25.08.15
 * Time: 11:45
 */

namespace Database;

class Database
{
    protected $conn;

    public function __construct()
    {
        $this->connection();
    }

    private function connection()
    {
        $connInfo = [
            'host' => 'localhost',
            'login' => 'root',
            'password' => '',
            'dbName' => 'ewidencja'
        ];

        $connection = new \mysqli($connInfo['host'],$connInfo['login'], $connInfo['password'], $connInfo['dbName']);

        if($connection->connect_error) {
            throw new \Exception('Blad polaczenia z baza danych! ' .$connection->connect_error, E_USER_ERROR);
        } else {
            $this->conn = $connection;
        }
    }

}