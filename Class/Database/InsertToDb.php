<?php
/**
 * Created by PhpStorm.
 * User: Komando
 * Date: 25.08.15
 * Time: 11:47
 */

namespace Database;

final class InsertToDb extends Database
{
    private $table;
    private $values = [];
    private $a_param_type= [];
    private $query;

    public function __construct(array $values, $table)
    {
        parent::__construct();
        $this->values = $values;
        $this->table = $table;
        $this->query();
        $this->paramType();
        $this->insert();
    }

    private function query()
    {
        $q = str_repeat('?,',count($this->values));
        $q = substr($q, 0, -1);

        $this->query = 'INSERT INTO '.$this->table. ' VALUES (null, '.$q.')';
    }

    private function paramType()
    {
        foreach($this->values as $value) {
            $type = gettype($value);
            array_push($this->a_param_type,substr($type,0,1));
        }
    }

    private function insert()
    {
        $a_params = [];
        $param_type = '';
        $n = count($this->a_param_type);
        for($i=0;$i<$n;$i++) {
            $param_type .= $this->a_param_type[$i];
        }

        $a_params[] = &$param_type;

        for($i=0;$i<$n;$i++) {
            $a_params[] = &$this->values[$i];
        }

        if($prep = $this->conn->prepare($this->query)) {
            call_user_func_array(array($prep, 'bind_param'), $a_params);
            $prep->execute();
        }
    }
}