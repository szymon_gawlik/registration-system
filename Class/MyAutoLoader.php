<?php
/**
 * Created by PhpStorm.
 * User: Komando
 * Date: 25.08.15
 * Time: 11:43
 */

class myAutoloader
{
    public static function autoload($class)
    {
        $class = str_replace('\\','/', $class);
        include_once "$class.php";
    }
}

spl_autoload_register(array('myAutoLoader','autoload'));