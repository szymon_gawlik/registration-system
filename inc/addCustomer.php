<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\InsertToDb;
use Database\SelectFromDb;
use Database\UpdateDb;
use Symfony\Component\HttpFoundation\Request;

try {
    $request = Request::createFromGlobals();

    $category = $request->request->get('category');
    $item = $request->request->get('item');
    $customer = $request->request->get('customer');
    $number = intval($request->request->get('number'));
    $value = intval($request->request->get('value'));

    if(empty($category) || empty($item) || empty($customer) || empty($number) || empty($value)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $categoryId = (new SelectFromDb('category',[],['name'=>$category]))->result[0];
    $products = (new SelectFromDb('product',['number'],['id'=>$item,'category'=>$categoryId['id']]))->result[0];
    if(empty($products)) {
        echo '{"error":"Nie ma takiego przedmiotu!"}';
        exit;
    }
    if($products['number'] < $number) {
        echo '{"error":"Brak wymaganej ilości przedmiotu!"}';
        exit;
    }

    if(new InsertToDb([$item,$number,$categoryId['id'],$customer,$value],'customers')) {
        $newNumber = $products['number'] - $number;
        new UpdateDb(['number'=>$newNumber],'product',$item);
        echo '{"error":"Poprawnie dodano!"}';
    }

} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}