<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\InsertToDb as Insert;
use Database\SelectFromDb as Select;

try {
    if(empty($_POST['name']) || empty($_FILES['image']['name']) || empty($_POST['desc'])) {
        if(empty($_POST['name'])) {
            $field = 'Nazwa';
        }
        if(empty($_FILES['image']['name'])) {
            $field = 'Obraz';
        }
        if(empty($_POST['desc'])) {
            $field = 'Opis';
        }
        echo '{"error":"<span class=\"glyphicon glyphicon-exclamation-sign\"></span> Pole <strong>'.$field.'</strong> jest obowiązkowe!"}';
        exit;
    }
    $sourcePath = $_FILES['image']['tmp_name'];
    $targetPath = '../web/uploads/'.$_FILES['image']['name'];

    move_uploaded_file($sourcePath, $targetPath);

    $products = (new Select('product', array('inventory_key'), array('category'=>$_POST['category'])))->result;
    $product_key = $products[(count($products)-1)]['inventory_key']+1;
    $insert = new Insert(array($_POST['name'],$_POST['desc'],$_POST['category'],0,$product_key,$_FILES['image']['name']),'product');
    $newProducts = (new Select('product', array('name','category','inventory_key','number','image','description')))->result;
    $newProduct = $newProducts[count($newProducts)-1];
    $category = (new Select('category', array('name','category_key'), array('id'=>$_POST['category'])))->result;

    $newProduct['category'] = $category[0]['name'];
    $newProduct['inventory_key'] = $category[0]['category_key'].sprintf("%'.05d\n",$newProduct['inventory_key']);

    echo json_encode($newProduct);
} catch (Exception $e) {
    $error = array('error' => $e->getMessage());
    echo json_encode($error);
}