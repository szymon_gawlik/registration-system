<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\UpdateDb as Update;
use Database\SelectFromDb as Select;

try {
    new Update(array('name' => $_POST['name'], 'category' => $_POST['category'],'number' => $_POST['number'], 'description' => $_POST['desc']),'product',$_POST['id']);

    $category = (new Select('category', array('name','category_key'), array('id'=>$_POST['category'])))->result;
    $newProducts = (new Select('product', array('name','category','inventory_key','number','image')))->result;
    $newProduct = $newProducts[count($newProducts)-1];
    $newProduct['inventory_key'] = $category[0]['category_key'].sprintf("%'.05d\n",$newProduct[0]['inventory_key']);
    $newProduct['category'] = $category[0]['name'];

    echo json_encode($newProduct);
} catch (Exception $e) {
    $data = array('message' => 'Błąd! '.$e->getMessage());
    echo json_encode($data);
}