<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\SelectFromDb;

try {
    $data = (new SelectFromDb('category', array('name')))->result;
    echo json_encode($data);
} catch (Exception $e) {
    $json = json_encode($e->getMessage());
}