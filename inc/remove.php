<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';
use Database\Delete;

try {
    new Delete('product', $_POST['id']);

    echo 'Poprawnie usunięto!';
} catch (Exception $e) {
    echo $e->getMessage();
}