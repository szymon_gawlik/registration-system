<?php

require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\UpdateDb as Update;
use Database\SelectFromDb as Select;

$data = (new Select('product', array('name','category','inventory_key','number'), array('id' => $_POST['id'])))->result;
new Update(array('number' => ($data[0]['number']+1)),'product',$_POST['id']);
$data[0]['number'] = $data[0]['number']+1;

echo json_encode($data);