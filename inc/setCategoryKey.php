<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\SelectFromDb;

try {
    $date = (new SelectFromDb('category', array('category_key')))->result;

    $keys = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'R',18=>'S',19=>'T',20=>'U',21=>'W',22=>'X',23=>'Y',24=>'Z');

    foreach($keys as $number => $key) {
        if($key == $date[count($date)-1]['category_key']) {
            $i = $number+1; break;
        }
    }
    echo $keys[$i];
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}