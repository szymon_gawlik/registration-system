<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\InsertToDb;
use Database\SelectFromDb;

try {
    new InsertToDb(array($_POST['name'],$_POST['key']),'category');
    $categories = (new SelectFromDb('category', array('name','category_key')))->result;
    $newCategory = $categories[count($categories)-1];
    echo json_encode($newCategory[0]);
} catch (Exception $e) {
    echo '{"error":"'.$e->getMessage().'"}';
}