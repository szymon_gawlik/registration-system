<?php

require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\SelectFromDb;

try {
    $customers = (new SelectFromDb('customers',['item','number','category','customer','value']))->result;
    $i = 0;
    foreach ($customers as $customer) {
        $itemName = (new SelectFromDb('product',['name'],['id'=>$customer['item']]))->result;
        $categoryName = (new SelectFromDb('category',['name'],['id'=>$customer['category']]))->result;

        $customers[$i]['item'] = $itemName[0]['name'];
        $customers[$i]['category'] = $categoryName[0]['name'];
        $i++;
    }

    echo json_encode($customers);
} catch (Exception $e) {
    echo '{"error": "'.$e->getMessage().'"}';
}