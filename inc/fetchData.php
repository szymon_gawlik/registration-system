<?php
require_once '../Class/MyAutoLoader.php';
require_once '../vendor/autoload.php';

use Database\SelectFromDb;

try {
    $data = (new SelectFromDb('product', ['name','category','inventory_key','number','image','description']))->result;
    $i=0;
    foreach($data as $value) {
        $category = (new SelectFromDb('category', ['name','category_key'], ['id'=>$value['category']]))->result;
        $data[$i]['category'] = $category[0]['name'];
        $data[$i]['inventory_key'] = $category[0]['category_key'].sprintf("%'.05d\n",$data[$i]['inventory_key']);
        if(!isset($data[$i]['number'])) {
            $data[$i]['number'] = 0;
        }
        $i++;
    }

    echo json_encode($data);
} catch (Exception $e) {
    echo $e->getMessage();
}
